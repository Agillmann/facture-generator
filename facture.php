<?php
//tableau clé valeur entreprise
$entreprise = array(
    'nom' => "Lego Industrie",
    'adresse' => "1, PASSAGE DE LA CANOPÉE",
    'cp' => "75001",
    'telephone' => "0149279425");

//tableau clé valeur client
$client = array(
    'id' => "NC".mt_rand(1000, 9999),
    'nom' => "Gillmann",
    'prenom' => "Adrien",
    'adresse' => "15 avenue Victor Hugo",
    'cp' => "94210");

//Tableau2dim produits
$produits = array(
array(
    'libelle' => "Lego Star wars Collection",
    'reference' => "0001",
    'quantite' => mt_rand(0,4),
    'prix_unitaire' => mt_rand(1,12)),
array(
    'libelle' => "Lego Avengers",
    'reference' => "0002",
    'quantite' =>  mt_rand(0,4),
    'prix_unitaire' => mt_rand(1,12)),
array(
    'libelle' => "Lego Dc Comics",
    'reference' => "0003",
    'quantite' =>  mt_rand(0,4),
    'prix_unitaire' =>  mt_rand(1,12)),
array(
    'libelle' => "Lego Retour vers le futue ",
    'reference' => "0004",
    'quantite' =>  mt_rand(0,4),
    'prix_unitaire' =>  mt_rand(1,12)),
array(
    'libelle' => "Lego World",
    'reference' => "0005",
    'quantite' =>  mt_rand(0,4),
    'prix_unitaire' =>  mt_rand(1,12))
);

//TVA en %
$tva = 20;


//Calcule total ht
function calcul_prix_total($tab_produits)
{
    $res = 0;
    for ($i=0; $i < count($tab_produits) ; $i++) {
        $res += $tab_produits[$i]["prix_unitaire"] * $tab_produits[$i]["quantite"];
    }
    return $res;
}

//Calcule tva en fonction du total ht et le taux tva exprimé en %
function calcul_prix_tva($ht, $taux_tva)
{
    return ($ht / 100 * $taux_tva);
}

//tableau html produit
function html_tab_produit($tab_produits){
    $res_html = "
    <table>
        <tr>
            <th>Référence</th>
            <th>Quantité</th>
            <th>Désignation</th>
            <th>Prix unititaire HT</th>
            <th>Prix total HT</th>
        </tr>";
    for ($i = 0; $i < count($tab_produits); $i++){
        if ($tab_produits[$i]["quantite"] != 0) {
            $res_html .= ("
                <tr>
                    <td>".$tab_produits[$i]["reference"]."</td>
                    <td>".$tab_produits[$i]["quantite"]."</td>
                    <td>".$tab_produits[$i]["libelle"]."</td>
                    <td>".$tab_produits[$i]["prix_unitaire"]."</td>
                    <td>".$tab_produits[$i]["prix_unitaire"] * $tab_produits[$i]["quantite"]."</td>
                </tr>
                ");
        }
    }
    $res_html .= "</table>";
    return $res_html;
}

//tableau html total
function html_tab_total($tab_produits,$tva){
    $total_ht = calcul_prix_total($tab_produits);
    $total_tva = calcul_prix_tva($total_ht, $tva);
    $total_ttc = $total_ht + $total_tva;
    $res_html = "
    <table class=\"tab-total\">
        <tr>
            <td>Total Hors Taxe</td>
            <td>".$total_ht." €</td>
        </tr>
        <tr>
            <td>TVA a 20%</td>
            <td>".$total_tva." €</td>
        </tr>
        <tr>
            <td>Total TTC</td>
            <td>".$total_ttc." €</td>
        </tr>
    </table>";
    return $res_html;
}
 ?>
