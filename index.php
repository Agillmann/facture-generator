<?php

include 'facture.php';
$html_tab_total = html_tab_total($produits,$tva);
$html_tab_produit = html_tab_produit($produits);
echo ('
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Facture Generator</title>
    </head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <body>
            <section class="apercu-facture">
                    <div class="bloc1">
                        <h1>Facture</h1>
                        <h2>'.$entreprise["nom"].'</span>
                    </div>
                    <div class="bloc2">
                        <span>'.$entreprise["adresse"].'</span>
                        <span>'.$entreprise["cp"].'</span>
                        <span>'.$entreprise["telephone"].'</span>
                    </div>
                    <div class="bloc3">
                        <div>
                            <span>Référence : RF'.mt_rand(1000,9999).'</span>
                            <span>Date : '.date("d M Y").'</span>
                            <span>N°client : '.$client["id"].'</span>
                        </div>
                        <div>
                            <span>'.$client["nom"].' '.$client["prenom"].'</span>
                            <span>'.$client["adresse"].'</span>
                            <span>'.$client["cp"].'</span>
                        </div>
                    </div>
                    <h3>Intitulé: Description du projet et/ou Produit facturé</h3>
                    '.$html_tab_produit.
                    $html_tab_total.'
                    <div>
                        <span>En votre aimable règlement</span>
                        <span>Cordialement</span>
                    </div>
                    <p>
                        Conditions de paiement : paiment à réception de facture, à 30 jours... Aucune escompte consenti pour règlement anticipé. Tout incident de paiment est passible d\'interêt de retard. le montant des pénalités résulte de l\'application aux sommes restant dues d\'un taux d\'interet légal en vigueur au moment de l\'incident.
                    </p>
                    <div>
                        <span>N° Siret 210.868.764 00015 RCS Montpellier</span>
                        <span>Code APE 947A - N° TVA Intracom FR77825896764000</span>
                    </div>
            </section>
        </main>
    </body>
</html>
');
?>
